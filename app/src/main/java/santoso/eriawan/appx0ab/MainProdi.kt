package santoso.eriawan.appx0ab

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.prodi.*
import kotlinx.android.synthetic.main.row_prodi.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap

class MainProdi : AppCompatActivity() , View.OnClickListener {

    lateinit var adapProdi: AdapterProdi
    var daftarprodi = mutableListOf<HashMap<String, String>>()
    var uri1 = "http://192.168.43.228/kampus2/show_prodi.php"
    var uri2 = "http://192.168.43.228/kampus2/query_prodi.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.prodi)

        adapProdi = AdapterProdi(daftarprodi, this)
        lsprodi.layoutManager = LinearLayoutManager(this)
        lsprodi.adapter = adapProdi

        btnInsProdi.setOnClickListener(this)
        btnDeleteProdi.setOnClickListener(this)
        btnUpProdi.setOnClickListener(this)

    }

    override fun onStart() {
        super.onStart()
        Showprodi()
    }


    fun Showprodi() {
        val request = StringRequest(Request.Method.POST, uri1, Response.Listener { response ->
            daftarprodi.clear()
            val jsonArray = JSONArray(response)
            for (x in 0..(jsonArray.length() - 1)) {
                val jsonObject = jsonArray.getJSONObject(x)
                var prodi = HashMap<String, String>()
                prodi.put("nama_prodi", jsonObject.getString("nama_prodi"))
                prodi.put("id_prodi", jsonObject.getInt("id_prodi").toString())
                daftarprodi.add(prodi)
            }
            adapProdi.notifyDataSetChanged()
        }, Response.ErrorListener { error ->
            Toast.makeText(this, "Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
        })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnInsProdi -> {
                query("insert")
                Showprodi()

            }
            R.id.btnUpProdi -> {
                query("update")
                Showprodi()

            }
            R.id.btnDeleteProdi -> {
                query("delete")
                Showprodi()
            }
        }
    }
    fun query(mode : String){
        val request = object : StringRequest(Method.POST,uri2,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    Showprodi()
                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung dengan server",Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nama_prodi",namaProdi.text.toString())

                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("nama",txProdi.text.toString())
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("nama_prodi",namaProdi.text.toString())

                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}