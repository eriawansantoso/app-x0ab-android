package santoso.eriawan.appx0ab

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.menu.*

class Menu : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu)
        btnMhs.setOnClickListener(this)
        btnProdi.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnMhs->{
                val ganti =  Intent(this,MainActivity::class.java)
                startActivity(ganti)
            }
            R.id.btnProdi->{
                val ganti = Intent(this, MainProdi::class.java)
                startActivity(ganti)

            }
        }
    }

}